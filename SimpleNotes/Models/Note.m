//
//  Note.m
//  SimpleNotes
//
//  Created by Claire Knight on 26/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import "Note.h"
#import <Parse/PFObject+Subclass.h>

@implementation Note

@dynamic author, text, updatedAt, createdAt, latitude, longitude, humanReadableLocation, isDraft;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Note";
}

@end
