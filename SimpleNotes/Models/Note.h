//
//  Note.h
//  SimpleNotes
//
//  Created by Claire Knight on 26/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import <Parse/Parse.h>
#import <CoreLocation/CoreLocation.h>

@interface Note : PFObject <PFSubclassing>

@property (nonatomic, strong) PFUser     *author;
@property (nonatomic, strong) NSString   *text;
@property (nonatomic, strong) NSDate     *createdAt;
@property (nonatomic, strong) NSDate     *updatedAt;
@property (nonatomic, strong) NSNumber   *latitude;
@property (nonatomic, strong) NSNumber   *longitude;
@property (nonatomic, strong) NSString   *humanReadableLocation;
@property (nonatomic)         BOOL       isDraft;

+ (NSString *)parseClassName;

@end
