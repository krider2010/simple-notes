//
//  NoteTableViewCell.m
//  SimpleNotes
//
//  Created by Claire Knight on 25/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import "NoteTableViewCell.h"
#import "UIColor+HexColours.h"

@implementation NoteTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if (selected) {
        self.backgroundColor = [UIColor colorWithHexString:@"#87FC70" withAlpha:0.2f];
    } else {
        self.backgroundColor = [UIColor whiteColor];
    }
}

@end
