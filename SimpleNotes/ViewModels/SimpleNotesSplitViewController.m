//
//  SimpleNotesSplitViewController.m
//  SimpleNotes
//
//  Created by Claire Knight on 25/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import "SimpleNotesSplitViewController.h"
#import "NoteDetailViewController.h"

@implementation SimpleNotesSplitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.delegate = self;
}

- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController  ontoPrimaryViewController:(UIViewController *)primaryViewController {
    
    // Always show the list in portrait on the phone
    return YES;
}

@end
