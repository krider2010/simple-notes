//
//  SimpleNotesSplitViewController.h
//  SimpleNotes
//
//  Created by Claire Knight on 25/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleNotesSplitViewController : UISplitViewController <UISplitViewControllerDelegate>

@end
