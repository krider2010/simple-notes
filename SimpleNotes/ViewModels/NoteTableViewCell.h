//
//  NoteTableViewCell.h
//  SimpleNotes
//
//  Created by Claire Knight on 25/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import "Note.h"
#import <UIKit/UIKit.h>

@interface NoteTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *summary;
@property (weak, nonatomic) IBOutlet UILabel *dateUpdatedLabel;

@end
