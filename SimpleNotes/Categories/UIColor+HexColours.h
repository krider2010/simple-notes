//
//  UIColor+HexColours.h
//  SimpleNotes
//
//  Created by Claire Knight on 26/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColours)

+ (UIColor *)colorWithHexString:(NSString *)hexString;
+ (UIColor *)colorWithHexString:(NSString *)hexString withAlpha:(CGFloat)alpha;
+ (NSString *)hexValuesFromUIColor:(UIColor *)color;

@end
