//
//  UIColor+HexColours.m
//  SimpleNotes
//
//  Created by Claire Knight on 26/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import "UIColor+HexColours.h"

@implementation UIColor (HexColours)

+(UIColor *)colorWithHexString:(NSString *)hexString {
    // Strip prefixed # hash (if there is one)
    if ([hexString hasPrefix:@"#"] && [hexString length] > 1) {
        hexString = [hexString substringFromIndex:1];
    }
    
    // Determine if 3 or 6 digits
    NSUInteger componentLength = 0;
    if ([hexString length] == 3) {
        componentLength = 1;
    } else if ([hexString length] == 6) {
        componentLength = 2;
    } else {
        return nil;
    }
    
    BOOL isValid = YES;
    CGFloat components[3];
    
    // Seperate the RGB values
    for (NSUInteger i = 0; i < 3; i++) {
        NSString *component = [hexString substringWithRange:NSMakeRange(componentLength * i, componentLength)];
        if (componentLength == 1) {
            component = [component stringByAppendingString:component];
        }
        
        NSScanner *scanner = [NSScanner scannerWithString:component];
        unsigned int value;
        isValid &= [scanner scanHexInt:&value];
        components[i] = (CGFloat)value / 255.0f;
    }
    
    if (!isValid) {
        return nil;
    }
    
    return [UIColor colorWithRed:components[0]
                           green:components[1]
                            blue:components[2]
                           alpha:1.0];
}

+ (UIColor *)colorWithHexString:(NSString *)hexString withAlpha:(CGFloat)alpha {
    return [[self colorWithHexString:hexString] colorWithAlphaComponent:alpha];
}

+(NSString *)hexValuesFromUIColor:(UIColor *)color {
    if (!color) {
        return nil;
    }
    
    if (color == [UIColor whiteColor]) {
        // Special case, as white doesn't fall into the RGB color space
        return @"ffffff";
    }
    
    CGFloat red;
    CGFloat blue;
    CGFloat green;
    CGFloat alpha;
    
    [color getRed:&red green:&green blue:&blue alpha:&alpha];
    
    int redDec = (int)(red * 255);
    int greenDec = (int)(green * 255);
    int blueDec = (int)(blue * 255);
    
    NSString *returnString = [NSString stringWithFormat:@"%02x%02x%02x", (unsigned int)redDec, (unsigned int)greenDec, (unsigned int)blueDec];
    
    return returnString;
}

@end
