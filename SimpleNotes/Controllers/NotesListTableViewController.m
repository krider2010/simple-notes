//
//  NotesListTableViewController.m
//  SimpleNotes
//
//  Created by Claire Knight on 25/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import "NotesListTableViewController.h"
#import "BTNavigationDropdownMenu-Swift.h"
#import "NoteTableViewCell.h"
#import "NoteDetailViewController.h"
#import "AppDelegate.h"
#import "UIColor+HexColours.h"
#import <CSNotificationView/CSNotificationView.h>
#import <ZZCustomAlertView/ZZCustomAlertView.h>
#import <DateTools/DateTools.h>
#import <ParseUI/ParseUI.h>


@interface NotesListTableViewController () <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>
@property (nonatomic, strong) NSMutableArray *notes;
@property (nonatomic, strong) NSArray *menuItems;
@end


static NSString *const noteCellReuseIdentifier = @"noteCellIdentifier";


@implementation NotesListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.notes = [NSMutableArray array];
    
    // Allow editing of the list; permits the D of CRUD
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    // Allow creation of new notes
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                          target:self
                                                                          action:@selector(addNewNote:)];
    self.navigationItem.rightBarButtonItem =  item;
    
    // Funky menu for other options, using a fork so they can don't replace VC content
    self.menuItems = @[@"Sync to Cloud", @"About"];

    // Adjust the frame based on the current visual dimensions
    CGRect frame = self.navigationController.navigationBar.frame;
    BTNavigationDropdownMenu *menu = [[BTNavigationDropdownMenu alloc] initWithFrame:frame
                                                                               title:@"Notes"
                                                                               items:self.menuItems
                                                                       containerView:self.view
                                      ];
    self.navigationItem.titleView = menu;
    
    // Treat the menu as a means of triggering segues, dialogs, or background tasks
    menu.showCheckmarks = [NSNumber numberWithBool:NO];
    menu.updateTitleOnSelection = [NSNumber numberWithBool:NO];
    menu.cellBackgroundColor = [UIColor colorWithHexString:@"1D62F0"];
    menu.cellTextLabelColor = [UIColor whiteColor];
    menu.cellSelectionColor = [UIColor colorWithHexString:@"#87FC70" withAlpha:0.2f];
    
    menu.didSelectItemAtIndexHandler = ^(NSInteger indexPath) {
        switch (indexPath) {
            case 0:
                [self handleSync];
                break;
                
            case 1:
                [self showAbout];
                break;
                
            default:
                // Do nothing!
                break;
        }
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateNotes];
    
    // If there is a logged in user, automatically sync notes to the server
    if ([PFAnonymousUtils isLinkedWithUser:[PFUser currentUser]]) {
        [self syncNotesToParse];
    }
}

- (void)updateNotes {
    PFQuery *query = [PFQuery queryWithClassName:@"Note"];
    [query fromLocalDatastore];
    [query orderByDescending:@"updatedAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            [self.notes removeAllObjects];
            [self.notes addObjectsFromArray:objects];
            [self.tableView reloadData];
        } else {
            NSLog(@"Unable to query for notes");
        }
    }];    
}

- (void)addNewNote:(id)sender {
    [self performSegueWithIdentifier:@"createNewNote" sender:sender];    
}

- (void)handleSync {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NetworkStatus status = [appDelegate checkForNetwork];
    
    if (status == ReachableViaWWAN || status == ReachableViaWiFi) {
        BOOL isAnonymous = [PFAnonymousUtils isLinkedWithUser:[PFUser currentUser]];
        if (!isAnonymous) {
            [self syncNotesToParse];
        } else {
            // Defaults of login, signup, submit, dismiss are fine
            // An extension would be to allow signup via Twitter or Facebook but not included
            // here because of the need to setup apps in those systems and join the dots.
            PFLogInViewController *logInController = [[PFLogInViewController alloc] init];
            logInController.delegate = self;
            logInController.emailAsUsername = YES;
            
            PFSignUpViewController *signUpViewController = [[PFSignUpViewController alloc] init];
            signUpViewController.delegate = self;
            signUpViewController.emailAsUsername = YES;
            
            [logInController setSignUpController:signUpViewController];
            
            [self presentViewController:logInController animated:YES completion:nil];
        }
    } else {
        [CSNotificationView showInViewController:self
                                           style:CSNotificationViewStyleError
                                         message:@"Sync is not possible until there is an internet connection. Try again later."];
    }
    
    
    // A note on syncing...
    /*
     Parse does not dictate any strategy and can support whatever is coded. Since this is
     simple demonstration app, then simple syncing logic is going to be used. Syncing is
     still a hard problem and I've spent many many hours on it for client projects :)
     
     On first login, the content stored locally is synced to Parse as the absolute truth.
     After this, the client version is always correct. This may cause issues if used
     extensively on two different devices, but that is where more development time, unit
     testing, and in-app testing come into play!
     */
}

- (void)syncNotesToParse {
    __weak id weakSelf = self;
    PFQuery *query = [PFQuery queryWithClassName:@"Note"];
    [query fromLocalDatastore];
    [query whereKey:@"isDraft" equalTo:[NSNumber numberWithBool:YES]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (Note *note in objects) {
                note.isDraft = NO;
                [note saveInBackgroundWithBlock:^(BOOL succeeded, NSError *saveError) {
                    if (saveError) {
                        note.isDraft = YES;
                        [note pinInBackground];
                        NSLog(@"Unable to save remote note: %@", saveError.description);
                        [weakSelf handleError:saveError];
                    }
                }];
            }
        } else {
            NSLog(@"Unable to query for notes: %@", error.description);
            [weakSelf handleError:error];
        }
    }];
}

- (void)loadFromParse {
    PFQuery *query = [PFQuery queryWithClassName:@"Note"];
    [query whereKey:@"author" equalTo:[PFUser currentUser]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            [PFObject pinAllInBackground:objects
                                   block:^(BOOL succeeded, NSError *saveError) {
                                       if (succeeded) {
                                           [self updateNotes];
                                       } else {
                                           NSLog(@"Unable to save notes locally");
                                       }
                                   }];
        } else {
            NSLog(@"Unable to load notes from Parse: %@", error.description);
        }
    }];
}

- (void)showAbout {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"About"
                                                    message:@"A simple app to demonstrate some basics of app development. Acknowledgements in the settings bundle. Images all public domain from Pixabay."
                                                   delegate:nil
                                          cancelButtonTitle:@"Close"
                                          otherButtonTitles: nil];
    [alert show];    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (long)[self.notes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NoteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:noteCellReuseIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[NoteTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:noteCellReuseIdentifier];
    }
    
    [self updateCell:cell forIndexPath:indexPath];
    
    return cell;
}

// Prefer this to using KVO in the table cell as used to be advocated, as this keeps the model
// knowledge away from the visual component.
- (void)updateCell:(NoteTableViewCell*) cell forIndexPath:(NSIndexPath *)indexPath {
    Note *note = [self.notes objectAtIndex:(unsigned long)indexPath.row];
    [cell.summary setText:note.text];
    [cell.dateUpdatedLabel setText:note.updatedAt.timeAgoSinceNow];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Note *itemToDelete = [self.notes objectAtIndex:(unsigned long)indexPath.row];
        [self.notes removeObjectAtIndex:(unsigned long)indexPath.row];
        [itemToDelete deleteEventually];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

// NOTE: Selection of the row is handled via storyboard setup


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"viewNote"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        UINavigationController *nc = [segue destinationViewController];
        
        NoteDetailViewController *noteDetailVC = nc.viewControllers[0];
        noteDetailVC.note = [self.notes objectAtIndex:(unsigned)indexPath.row];
        
        // Nicety to expand the detail view to full screen on some devices (iOS 8+ only)
        noteDetailVC.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        noteDetailVC.navigationItem.leftItemsSupplementBackButton = true;
    }
}


#pragma mark - PFLogInViewControllerDelegate

- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [self handleUserJoin:user];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(PFUI_NULLABLE NSError *)error {
    [CSNotificationView showInViewController:self
                                       style:CSNotificationViewStyleError
                                     message:@"Uh oh! Unable to login. Please try again later."];
    NSLog(@"Failed to login to Parse: %@", error.description);
}

- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    [CSNotificationView showInViewController:self
                                       style:CSNotificationViewStyleError
                                     message:@"Oops! You cancelled login. Notes cannot be synced without an account being created."];
}


#pragma mark - PFSignUpViewControllerDelegate


- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user
{
    [self handleUserJoin:user];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error
{
    [CSNotificationView showInViewController:self
                                       style:CSNotificationViewStyleError
                                     message:@"Uh oh! Unable to signup. Please try again later."];
    NSLog(@"Failed to signup to Parse: %@", error.description);
    
}


- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController
{
    [CSNotificationView showInViewController:self
                                       style:CSNotificationViewStyleError
                                     message:@"Oops! You cancelled signup. Notes cannot be synced without an account being created."];
}


#pragma mark - PF Utilities

- (void)handleUserJoin:(PFUser *)user {
    if ([user isNew]) {
        [self syncNotesToParse];
    } else {
        [self loadFromParse];
    }
}

- (void)handleError:(NSError *)error {
    if (![error.domain isEqualToString:PFParseErrorDomain]) {
        return;
    }
    
    switch (error.code) {
        case kPFErrorInvalidSessionToken: {
            [self handleInvalidSessionTokenError];
            break;
        }
        default:
            break;
            
            // Consider handling other Parse API error conditions
    }
}

- (void)handleInvalidSessionTokenError {
    PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
    logInViewController.emailAsUsername = YES;
    logInViewController.delegate = self;
    [self presentViewController:logInViewController animated:YES completion:nil];
}


@end
