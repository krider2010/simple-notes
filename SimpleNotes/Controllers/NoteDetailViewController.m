//
//  NoteDetailViewController.m
//  SimpleNotes
//
//  Created by Claire Knight on 25/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import "NoteDetailViewController.h"
#import "EditNoteViewController.h"
#import "UIColor+HexColours.h"
#import "AppDelegate.h"
#import <ZZCustomAlertView/ZZCustomAlertView.h>
#import <MapKit/MapKit.h>
#import <CSNotificationView/CSNotificationView.h>
#import <DateTools/DateTools.h>

@interface NoteDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *dateCreatedLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateUpdatedLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UITextView *text;

@end


@implementation NoteDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.note) {
        if (self.note.createdAt) {
            [self.dateCreatedLabel setText:self.note.createdAt.timeAgoSinceNow];
        } else {
            [self.dateCreatedLabel setText:@"No creation date"];
        }
        if (self.note.updatedAt) {
            [self.dateUpdatedLabel setText:self.note.updatedAt.timeAgoSinceNow];
        } else {
            [self.dateUpdatedLabel setText:@"No updated date"];
        }
        if (self.note.text) {
            [self.text setText:self.note.text];
        }
        if (self.note.humanReadableLocation) {
            [self.locationLabel setText:self.note.humanReadableLocation];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"editExistingNote"]) {
        EditNoteViewController *vc = [segue destinationViewController];
        vc.note = self.note;
    }
}


#pragma mark - IBActions

- (IBAction)showMap:(id)sender {
    if (self.note && self.note.latitude) {
        MKMapView *mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, 280, 280)];
        mapView.mapType = MKMapTypeStandard;
        mapView.zoomEnabled = YES;
        mapView.scrollEnabled = YES;
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([self.note.latitude doubleValue], [self.note.longitude doubleValue]);
        mapView.centerCoordinate = coord;

        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        ZZCustomAlertView *alert = [ZZCustomAlertView alertViewWithParentView:appDelegate.window andContentView:mapView];
        alert.shouldBlurBackground = YES;
        alert.allowTapBackgroundToDismiss = YES;
        alert.shadowColor = [UIColor colorWithHexString:@"#87FC70"];
        alert.shadowAlpha = 0.1f;
        [alert show];
    } else {
        [CSNotificationView showInViewController:self
                                           style:CSNotificationViewStyleError
                                         message:@"This note does not have a location!"];
    }
    
}

@end
