//
//  NoteDetailViewController.h
//  SimpleNotes
//
//  Created by Claire Knight on 25/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import "Note.h"
#import <UIKit/UIKit.h>

@interface NoteDetailViewController : UIViewController

@property (strong, nonatomic) Note *note;

@end
