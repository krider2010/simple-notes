//
//  AppDelegate.h
//  SimpleNotes
//
//  Created by Claire Knight on 7/25/15
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import <Reachability/Reachability.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (NetworkStatus)checkForNetwork;

@end
