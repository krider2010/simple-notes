//
//  AppDelegate.m
//  SimpleNotes
//
//  Created by Claire Knight on 7/25/15
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import "AppDelegate.h"
#import "UIColor+HexColours.h"
#import "Note.h"
#import <Parse/Parse.h>
#import <ParseCrashReporting/ParseCrashReporting.h>
#import <Keys/SimplenotesKeys.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Keys for external services
    SimplenotesKeys *keys = [[SimplenotesKeys alloc] init];
    
    // Parse local data store to enable offline use, including anonymouse users unless logged in
    [Parse enableLocalDatastore];
    [PFUser enableAutomaticUser];
    
    // Custom class for model objects stored in the cloud
    [Note registerSubclass];
    
    // Crashes (removes the need for Fabric/Crashlytics)
    [ParseCrashReporting enable];
    
    // Connect to Parse
    [Parse setApplicationId:keys.parseApplicationId
                  clientKey:keys.parseClientKey];
    [PFUser enableRevocableSessionInBackground];
    
    // Analytics (avoiding need for GA/Facebook Analytics in a simple app)
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
 
    [self styleUI];
    
    return TRUE;
}

- (void)styleUI {
    // Light status bar is set on the project settings (Info.plist and General config)
    
    // Universally set the app int for buttons; simple app, simple colours
    [self.window setTintColor:[UIColor colorWithHexString:@"#87FC70"]];

    // Nav bar tint colour
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithHexString:@"#1D62F0"]];

    // Nav bar text colour
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
    // Other colourings set in the storyboard, or view controller code as appropriate
    // Grey background #F5F5F5
    // Text #FFFFFF or #000000
}

// Custom Fonts
//     Roboto
//       Roboto-Thin
//       Roboto-Italic
//       Roboto-BlackItalic
//       Roboto-Light
//       Roboto-BoldItalic
//       Roboto-LightItalic
//       Roboto-ThinItalic
//       Roboto-Black
//       Roboto-Bold
//       Roboto-Regular
//       Roboto-Medium
//       Roboto-MediumItalic
//     Roboto Condensed
//       RobotoCondensed-Bold
//       RobotoCondensed-Light
//       RobotoCondensed-BoldItalic
//       RobotoCondensed-Italic
//       RobotoCondensed-Regular
//       RobotoCondensed-LightItalic

- (NetworkStatus)checkForNetwork
{
    Reachability *myNetwork = [Reachability reachabilityWithHostname:@"google.co.uk"];
    return [myNetwork currentReachabilityStatus];
}

@end
