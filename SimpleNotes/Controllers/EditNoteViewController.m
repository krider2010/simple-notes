//
//  EditNoteViewController.m
//  SimpleNotes
//
//  Created by Claire Knight on 25/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import "EditNoteViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <CSNotificationView/CSNotificationView.h>


@interface EditNoteViewController () <CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spaceToBottomLayoutConstraint;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLGeocoder *geocoder;

@end


@implementation EditNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Location tracking
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    self.geocoder = [[CLGeocoder alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.noteText setText:@""];
    
    if (self.note == nil) {
        self.note = [Note object];
        self.note.author = [PFUser currentUser];
        self.note.isDraft = YES;  // Used to track syncing to the server
        self.note.createdAt = [NSDate date];
        self.note.updatedAt = [NSDate date];
        
        // Location tracking for new notes
        [self.locationManager requestWhenInUseAuthorization];  // iOS 8+ method

        if ([CLLocationManager locationServicesEnabled]) {
            [self.locationManager startUpdatingLocation];
        } else {
            NSLog(@"Location services is not enabled");
        }
    } else {
        [self.noteText setText:self.note.text];
        [self.locationLabel setText:self.note.humanReadableLocation];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    // Turn off location tracking once leaving this screen; no point keeping it running
    // and annoying the user!
    [self.locationManager stopUpdatingLocation];
    
    [super viewDidDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    [super viewWillDisappear:animated];
}


#pragma mark - Keyboard Handling

- (void)keyboardWillShow:(NSNotification *)notification {
    [self updateBottomLayoutConstraintWithNotification:notification];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [self updateBottomLayoutConstraintWithNotification:notification];
}

- (void)updateBottomLayoutConstraintWithNotification:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;

    double animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardEndFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect convertedEndFrame = [self.view convertRect:keyboardEndFrame fromView:self.view.window];
    NSUInteger animationCurve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue];
    
    self.spaceToBottomLayoutConstraint.constant = CGRectGetMaxY(self.view.bounds) - CGRectGetMinY(convertedEndFrame) + 12.f;  // 12.f from the storyboard
    
    [UIView animateWithDuration:animationDuration
                          delay:0.0f
                        options:animationCurve
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}


#pragma mark - IBActions

- (IBAction)cancelTouched:(id)sender {
    self.note = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveTouched:(id)sender {
    NSString *enteredText = [self.noteText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([enteredText length] == 0) {
        [CSNotificationView showInViewController:self
                                           style:CSNotificationViewStyleError
                                         message:@"You can't save an empty note! If you don't want to enter anything, use the Cancel button to get back to the note list."];
        return;
    }
    
    self.note.updatedAt = [NSDate date];
    self.note.text = self.noteText.text;
    [self.note pinInBackground];
    
    // Some basic analytic information, out of curiosity!
    if (self.note.latitude != nil) {
        CLLocationCoordinate2D coords = CLLocationCoordinate2DMake([self.note.latitude doubleValue], [self.note.longitude doubleValue]);
        NSNumber *latitide = [NSNumber numberWithDouble:coords.latitude];
        NSNumber *longitude = [NSNumber numberWithDouble:coords.longitude];
        NSDictionary *dimensions = @{
                                     @"category": @"note",
                                     @"location": [NSString stringWithFormat:@"%@, %@", [latitide stringValue], [longitude stringValue]]
                                     };
        [PFAnalytics trackEvent:@"read" dimensions:dimensions];
    } else {
        NSDictionary *dimensions = @{
                                     @"category": @"note",
                                     @"location": @"unknown"
                                     };
        [PFAnalytics trackEvent:@"read" dimensions:dimensions];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [CSNotificationView showInViewController:self
                                       style:CSNotificationViewStyleError
                                     message:@"Unable to get your location. Ensure you have enabled it in settings and have GPS coverage. Notes will not be tagged with location details."];

    NSLog(@"Error getting location: %@", error.description);
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    // Note: this will continue to update the location of the note if the user moves
    // around while creating it. This is a known "feature".
    
    CLLocation *location = [locations lastObject];
    
    CLLocationCoordinate2D coords = location.coordinate;
    NSNumber *latitide = [NSNumber numberWithDouble:coords.latitude];
    NSNumber *longitude = [NSNumber numberWithDouble:coords.longitude];
    self.note.latitude = latitide;
    self.note.longitude = longitude;
    self.note.humanReadableLocation = [NSString stringWithFormat:@"%@, %@", [latitide stringValue], [longitude stringValue]];
    
    [self.geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil && [placemarks count] > 0) {
            CLPlacemark *placemark = [placemarks lastObject];
            
            if ([placemark.subLocality length] != 0) {
                self.note.humanReadableLocation = placemark.subLocality;
            } else if ([placemark.locality length] != 0) {
                self.note.humanReadableLocation = placemark.locality;
            }
            
            [self.locationLabel setText:self.note.humanReadableLocation];
        } else {
            NSLog(@"Error looking up location: %@", error.debugDescription);
            
            // Default to the lat/long already set
            [self.locationLabel setText:self.note.humanReadableLocation];
        }
    }];
}

@end
