//
//  EditNoteViewController.h
//  SimpleNotes
//
//  Created by Claire Knight on 25/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import "Note.h"
#import <UIKit/UIKit.h>

@interface EditNoteViewController : UIViewController

@property (strong, nonatomic) Note *note;

@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UITextView *noteText;

@end
