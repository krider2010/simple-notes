//
//  main.m
//  SimpleNotes
//
//  Created by Claire Knight on 7/25/15
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

@import UIKit;

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
