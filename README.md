# SimpleNotes #

## Setup ##

Run `bin/setup`

This will:

 - Install the gem dependencies
 - Install the pod dependencies
 - Prompt for required keys (e.g. Parse, Fabric)

## Testing ##

Run `bin/test`

This will run the tests from the command line, and pipe the result through
[XCPretty][].

[XCPretty]: https://github.com/supermarin/xcpretty

## General Information ##

This app is not bullet-proof, nor has it got a sophisticated syncing model (as documented in the code). This is the product of about a day of work, including learning to use the Parse Data API, cocoapods-keys, and liftoff. Basic UI code is fairly standard, with some custom components. All views use auto-layout and size classes, and the app is Universal.

Please excuse the artwork for the AppIcon. I am clearly not a designer!

If you download this, you will need to create a Parse app to run it against. It is free and easy. A dev app will do. The setup above will prompt you for the appropriate keys.

Known enhancements:

* Allowing login via social accounts; Facebook and Twitter
* Implementing oauth access so Google could be used for login
* Localisation has not been done with strings in the code
* Refining the UI when in landscape large device mode to auto-select views properly
* Using a more sophisticated syncing mechanism to better deal with possible clashes from a user being logged in on different devices at the same time
* Using markup (or HTML) when writing notes
* There needs to be a lot more resilience from Parse issues; some basic session handling is included but that is it
* Many more unit tests are needed

If you want to get in touch then try [@krider2010](http://twitter.com/krider2010).