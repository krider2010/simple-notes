//
//  NoteTests.m
//  SimpleNotes
//
//  Created by Claire Knight on 26/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "Note.h"

@interface NoteTests : XCTestCase
@property (nonatomic) Note *note;

@end

@implementation NoteTests

- (void)setUp {
    [super setUp];
    self.note = [Note object];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testText {
    NSString *testString = @"Some random string for the test case";
    self.note.text = testString;
    
    XCTAssertEqualObjects(self.note.text, testString);
}

@end
