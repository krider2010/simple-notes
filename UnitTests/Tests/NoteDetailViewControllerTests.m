//
//  NoteDetailViewControllerTests.m
//  SimpleNotes
//
//  Created by Claire Knight on 26/07/2015.
//  Copyright (c) 2015 Moggy Tech Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "NoteDetailViewController.h"

@interface NoteDetailViewControllerTests : XCTestCase
@property (nonatomic) NoteDetailViewController *vcToTest;
@end

@implementation NoteDetailViewControllerTests

- (void)setUp {
    [super setUp];
    self.vcToTest = [[NoteDetailViewController alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// TODO: view testing is needed for this VC
- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
